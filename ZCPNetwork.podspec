Pod::Spec.new do |spec|

  spec.name          = "ZCPNetwork"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPNetwork"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPNetwork.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPNetwork."
  spec.description   = <<-DESC
                       ZCP ZCPNetwork. pa hao fang router
                       DESC

  spec.platform      = :ios, '8.0'
  spec.ios.deployment_target = '8.0'
  spec.module_name   = 'ZCPNetwork'
  spec.framework     = 'Foundation', 'UIKit'
  spec.source_files  = "ZCPNetwork/ZCPNetwork/**/*.{h,m}"

  spec.dependency 'ZCPGlobal', '~>0.0.1'

end
